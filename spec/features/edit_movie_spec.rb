require 'rails_helper'

descibe "Editing a movie" do
  it "updates the movie and shows the movie's updated details" do
    movie = Movie.create(movie_attributes)

    visit movie_url(movie)

    click_link 'Edit'

    expect(current_path).to eq(edit_movie_path(movie))

    expect(find_field('Title').value).to eq(movie.title)

    fill_in 'Title', with: "Updated Movie Title" #fill_in takes the name of a form field ("Title" in this case) and simulates a user filling in that field with the given value.

    click_button 'Update Movie' 

    expect(current_path).to eq(movie_path(movie))

    expect(page).to have_text('Updated Movie Title')
  end

  it "does not update the movie if it's invalid" do
    movie = Movie.create(movie_attributes)

    visit edit_movie_url(movie)

    fill_in 'Title', with: " "

    click_button 'Update Movie' 
      
    expect(page).to have_text('error')
  end
end
